import json

import requests


URL = 'https://angel.co/graphql?fallbackAOR=talent'

with open(f'payload_settings/payload_companies.json', 'r', encoding="utf-8") as f:
    PAYLOAD_JOBS = json.load(f)

with open(f'payload_settings/payload_people.json', 'r', encoding="utf-8") as f:
    PAYLOAD_PEOPLE = json.load(f)

with open(f'payload_settings/headers.json', 'r', encoding="utf-8") as f:
    HEADERS = json.load(f)


def parse_angel_companies(page=1, payload=PAYLOAD_JOBS):
    payload['variables']['filterConfigurationInput']['page'] = page
    response = requests.post(URL, headers=HEADERS, json=payload)
    print(f'-parse_angel_companies-{page}-', response.status_code)
    if response.status_code == 200:
        new_data = response.json()
        new_data = new_data.get('data', {}).get('talent', {}).get('jobSearchResults', {})
        new_data = new_data.get('startups', {}).get('edges', [])
        return new_data
    return []


def parse_angel_people(slug, payload=PAYLOAD_PEOPLE):
    payload['variables']['startupSlug'] = slug
    response = requests.post(URL, headers=HEADERS, json=payload)
    print(f'--parse_angel_people--{slug}-', response.status_code)
    if response.status_code == 200:
        return response.json()
    return {}


if __name__ == "__main__":
    import time

    RESULT_COMPANIES = []
    for page in range(1, 3):
        RESULT_COMPANIES.append(parse_angel_companies(page=page))
        time.sleep(.1)
    with open(f'loaded/parse_angel_companies.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(RESULT_COMPANIES, indent=4, sort_keys=True, ensure_ascii=False))

    RESULT_PEOPLE = {}
    for d in RESULT_COMPANIES[0]:
        slug = dict(d)['node']['slug']
        RESULT_PEOPLE = parse_angel_people(slug)
    with open(f'loaded/parse_angel_people.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(RESULT_PEOPLE, indent=4, sort_keys=True, ensure_ascii=False))
