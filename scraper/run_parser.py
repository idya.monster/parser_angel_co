import json
from time import sleep
from datetime import datetime

from scraper.get_angel_co import parse_angel_people, parse_angel_companies
from scraper.get_rocketreach_co import get_rocket_search, get_rocket_profile

ROCKET_SLEEP = .01


def make_pretty_data(company_data):
    def get_location(location_tags):
        return [location['displayName'] for location in location_tags]

    def time_format(time):
        return datetime.fromtimestamp(time).strftime("%d-%B-%Y %H:%M:%S %A")

    def get_jobs(job_list):
        res_list = []
        for job in job_list:
            res_list.append({
                'compensation': job['compensation'],
                'title': job['title'],
                'remote': job['remote'],
                'type': job['jobType'],
                'posted': time_format(job['liveStartAt']),
                'time': job['liveStartAt'],
            })
        return res_list

    def get_last_job(job_list):
        last = max([job['liveStartAt'] for job in job_list])
        return time_format(last)

    d = dict(company_data)
    company_slug = d['node']['slug']
    pretty_data = {
        "angel_slug": company_slug,
        'angel_link': f'https://angel.co/company/{company_slug}',
        'company_title': d['node']['name'],
        'company_location': get_location(d['node']['locationTaggings']),
        'company_size': d['node']['companySize'],
        'company_jobs_count': d['node']['jobListingsCount'],
        'company_jobs_list': get_jobs(d['node']['highlightedJobListings']),
        'company_last_job_publish': get_last_job(d['node']['highlightedJobListings']),
        "company_people": [],
        "company_small_description": d['node']['highConcept'],
    }
    return pretty_data


def parse_companies_list(current_page=1, page_limit=None):
    companies_list = []
    new_data = True
    while new_data:
        new_data = parse_angel_companies(page=current_page)
        companies_list.extend(
            [make_pretty_data(company) for company in new_data]
        )
        current_page += 1
        if page_limit and current_page > page_limit:
            new_data = False
    return companies_list


def load_people(data_list):
    def make_pretty_phones(phones_list):
        return [phone['number'] for phone in phones_list]

    result = []
    for data in data_list:
        people_list = parse_angel_people(data['angel_slug'])
        founders_list = people_list.get('data', {}).get('startup', {}).get('currentFounderRoles', [])
        people_result = []
        for people in founders_list:
            sleep(ROCKET_SLEEP)
            rocket_search = get_rocket_search(data['company_title'], people['user']['name'])
            r_user = rocket_search.get('profiles', [])
            r_profile = r_user[0] if r_user else {}
            people_result.append({
                "role_title": people['roleDisplayName'],
                "username": people['user']['name'],
                "angel_slug": people['user']['slug'],
                "angel_url": f"https://angel.co/{people['user']['slug']}",
                "rocket_profile_id": r_profile.get('id'),
                "location": r_profile.get('location'),
                "linkedin_url": r_profile.get('linkedin_url'),
                "emails": r_profile.get('teaser', {}).get('emails'),
                "office_phones": r_profile.get('teaser', {}).get('office_phones'),
                "phones": make_pretty_phones(r_profile.get('teaser', {}).get('phones', [])),
            })
            data['company_people'] = people_result
            result.append(data)
    return result


def save_to_json_file(data_json, file_name):
    with open(f'loaded/{file_name}.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(data_json, ensure_ascii=False, indent=4, sort_keys=True))


if __name__ == "__main__":
    pretty_data_list = parse_companies_list(page_limit=3)
    save_to_json_file(pretty_data_list, 'first_data')
    all_data = load_people(pretty_data_list)
    save_to_json_file(all_data, 'ALL_DATA')
