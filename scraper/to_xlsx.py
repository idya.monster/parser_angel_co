import json
from pprint import pformat

import xlsxwriter


def save_as_xlsl(data_list, file_name):
    companies_list = [
        'angel_link',
        'company_title',
        'company_small_description',
        'company_jobs_count',
        'company_last_job_publish',
        'company_location',
        'company_size',
    ]
    people_list = [
        'username',
        'role_title',
        'location',
        'emails',
        'office_phones',
        'phones',
        'linkedin_url',
        'angel_url',

    ]

    title_list = companies_list + people_list

    workbook = xlsxwriter.Workbook(f'{file_name}.xlsx', options={'strings_to_urls': True})
    worksheet = workbook.add_worksheet()

    # create header
    for company in range(len(title_list)):
        worksheet.write(0, company, title_list[company])

    # adding data
    abs_row = 1
    for company in data_list:
        for people in range(len(company['company_people'])):
            abs_row += 1
            for key in title_list:
                if key in companies_list:
                    if people == 0:
                        data_item = company[key]
                    else:
                        data_item = ".."
                else:
                    data_item = company['company_people'][people][key]
                worksheet.write(
                    abs_row,
                    title_list.index(key),
                    pformat(data_item)
                )
    workbook.close()


if __name__ == "__main__":
    with open(f'loaded/ALL_DATA.json', 'r', encoding="utf-8") as f:
        save_as_xlsl(
            json.load(f),
            file_name='ALL_DATA_NEW'
        )
    print('CONVERT OK')
