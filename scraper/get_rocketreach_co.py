import requests

# API_KEY = '451f9fka363a44a93407b1798ca8a514216aaee'
API_KEY = '45706ckf35ee07f60356505cb798c4e35813903'


def get_rocket_search(username, company_name=None):
    url = 'https://api.rocketreach.co/v1/api/search'
    params = {'api_key': API_KEY, 'keyword': username}
    if company_name:
        params['company']: company_name
    response = requests.get(url, params=params)
    print(f'---get_rocket_search--{username}--{company_name}-', response.status_code)
    if response.status_code == 200:
        return response.json()
    return dict()


def get_rocket_profile(profile_id):
    url = 'https://api.rocketreach.co/v1/api/lookupProfile'
    response = requests.get(url, params={'api_key': API_KEY, 'id': profile_id})
    print(f'---get_rocket_profile--{profile_id}-', response.status_code)
    if response.status_code == 200:
        return response.json()
    return dict()
